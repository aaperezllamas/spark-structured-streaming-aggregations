import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}

import scala.io.Source

/**
  * A Spark Structured Streaming application to split lines into words, lowercase them and filter out stopwords
  * to finally show in console the top twenty most common words found since the beginning
  */
object Top20Words {

  // TODO set your user name here
  val user: String = "aalvarez"

  val InputTopic = "streaming.books"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"Top20Words - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  /**
    * Reads the file specified as the params and returns its content as a list of lines
    * @param filePath
    * @return
    */
  def getFileLines(filePath: String): List[String] = {
    val source = Source.fromFile(filePath)
    try {
      source.getLines().toList
    }
    finally {
      source.close()
    }
  }

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "latest")
      .load()
      .select(decode('value, "UTF-8") as 'line)
  }

  // TODO implement this method (Receives a dataframe with a single column 'word')
  def top20words(inputTable: DataFrame): DataFrame =
    inputTable.groupBy('word).agg(count('word) as "total").orderBy(desc("total"))



  def main(args: Array[String]): Unit = {
    require(args.length == 1, "Top20Words <stop words file>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")
    val inputTable = getInputTableFromTopic(InputTopic)

    val stopWords = getFileLines(args(0))

    val cleanedWords = inputTable
        .withColumn("cleaned_line", regexp_replace('line, "[^a-zA-Z0-9']", " "))
        .select(explode(split('cleaned_line, """\s+""")) as 'word)
        .withColumn("lowercase_word", lower('word))
        .drop('word)
        .where(! 'lowercase_word.isin(stopWords: _*))
        .withColumnRenamed("lowercase_word","word")

    val resultTable = top20words(cleanedWords)

    val streamingQuery = resultTable
      .writeStream
      .outputMode(OutputMode.Complete) // OUTPUT MODE COMPLETE
      .format("console")
      .option("numRows", 20)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/aggregation/words")
      .trigger(Trigger.ProcessingTime("5 seconds"))
      .start()

    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}