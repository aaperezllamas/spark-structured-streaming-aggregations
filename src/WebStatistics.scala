import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.io.Source

/**
  * A Spark Structured Streaming application to parse, filter and aggregate NginX access log lines
  */
object WebStatistics {

  // TODO set your user name here
  val user: String = "aalvarez"

  val InputTopic = "streaming.logs.nginx"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"WebStatistics - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "earliest") // we want to classify all the existing logs in the topic
      .load()
      .select(decode('value, "UTF-8") as 'line)
  }


  // returns a dataframe with columns ts, origin_ip and url
  def parseAndFilterLogs(inputTable: DataFrame): DataFrame = {
    val regex = """^([0-9\.]+) - - \[(\S+) \+0000\] \"\w+ (\S+) HTTP\/1\.1\" \d+ \d+ .*"""

    inputTable
      .select(
        regexp_extract('line, regex, 2) as 'ts,
        regexp_extract('line, regex, 1) as 'origin_ip,
        regexp_extract('line, regex, 3) as 'url)
      .where('url.endsWith(".html"))
      .withColumn("ts", to_timestamp('ts, "dd/MMM/yyyy:HH:mm:ss"))
      .withWatermark("ts", "0 minutes")
  }

  // TODO implement this method. Should return a dataframe with start_time, end_time, path and ocurrences
  def aggregateLogs(logs: DataFrame): DataFrame = logs
      .groupBy(window('ts, "10 minutes") as 'window, 'url)
      .agg(count('url))
      .select(
        'window.getField("start") as 'start_time,
        'window.getField("end") as 'end_time,
        'url,
        'count
      )


  def main(args: Array[String]): Unit = {
    require(args.length == 1, "WebStatistics <output HDFS file path>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")
    val it = getInputTableFromTopic(InputTopic)

    val logsDF = parseAndFilterLogs(it)

    val resultTable = aggregateLogs(logsDF)

    val path = args(0)

    logger.info(s"Writing result to parquet file: $path")

    val streamingQuery = resultTable
      .writeStream
      .outputMode(OutputMode.Complete) // OUTPUT MODE APPEND as HDFS does not allow updates
      .format("parquet")
      .option("path", path)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/aggregation/logs")
      .trigger(Trigger.ProcessingTime("30 seconds"))
      .start()


    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}