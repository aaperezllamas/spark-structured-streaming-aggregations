# Practica 3 - Spark Structured Streaming - Aggregations

## Ejercicio Top20Words

### Objetivo

El objetivo de este ejercicio es continuar el procesamiento de palabras que comenzamos en el ejercicio 
`SplitAndCleanLines` de la Práctica 2 y realizar agregaciones sin ventanas sobre las palabras una vez filtradas con
los requisitos de la mencionada práctica.

En particular, vamos a mostrar por consola, en cada trigger, la lista de las 20 palabras más comunes encontradas 
hasta el momento junto a la lista de sus ocurrencias.


El pipeline de transformación se codificará empleando únicamente el API de Spark SQL (Dataframes + funciones sql)

La referencia de estas funciones se puede encontrar en el siguiente enlace de la documentación oficial:

https://spark.apache.org/docs/2.3.1/api/scala/index.html#org.apache.spark.sql.functions$

## Compilación y empaquetado

Para compilar y empaquetar la aplicación, ejecutar desde este mismo directorio el siguiente comando:

```bash
mvn clean package
```

También se puede ejecutar la tarea Maven desde el IDE (vista Maven -> Lifecycle -> package)

## Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-aggregations-0.1.0-SNAPSHOT.jar edge:

spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.Top20Words \
spark-structured-aggregations-0.1.0-SNAPSHOT.jar <stopwords file path>
```

## Ejercicio WebStatistics

### Objetivo

El objetivo de este ejercicio es generar una tabla parquet de estadísticas de acceso a la web de la documentacion.
Se requiere presentar el número de accesos a cada url agrupados por tramos de 10 minutos.


El esquema de la tabla generada debe ser el siguiente:

| start_time (string) | end_time (string) | URL             | count |
|---------------------|-------------------|-----------------|-------|
| 2019-03-19 19:10    | 2019-03-19 9:20   | /org/apache/... | 3     |


El pipeline de transformación se codificará empleando únicamente el API de Spark SQL (Dataframes + funciones sql)

La referencia de estas funciones se puede encontrar en el siguiente enlace de la documentación oficial:

https://spark.apache.org/docs/2.3.1/api/scala/index.html#org.apache.spark.sql.functions$

Para _aplanar_ una columna de tipo `struct` (como la columna _**window**_) se debe emplear el metodo `getField` de `Column`:
http://edge01.bigdata.alumnos.upcont.es:8088/#org.apache.spark.sql.Column

## Compilación y empaquetado

Para compilar y empaquetar la aplicación, ejecutar desde este mismo directorio el siguiente comando:

```bash
mvn clean package
```

También se puede ejecutar la tarea Maven desde el IDE (vista Maven -> Lifecycle -> package)

## Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-aggregations-0.1.0-SNAPSHOT.jar edge:

spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.WebStatistics \
spark-structured-aggregations-0.1.0-SNAPSHOT.jar <HDFS parquet file path>
```